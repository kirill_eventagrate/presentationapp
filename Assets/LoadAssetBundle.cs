﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class LoadAssetBundle : MonoBehaviour
{
    AssetBundle myLoadedAssetBundle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1)) {
            //myLoadedAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.persistentDataPath, "AssetBundles"));
            StartCoroutine(DownLoadAssetBundle());
        }
    }

    IEnumerator DownLoadAssetBundle()
    {
        WWW download;
        //string url = "file:///" + Path.Combine(Application.persistentDataPath, "presentationmodels");
        string url = Path.Combine(Application.persistentDataPath, "presentationmodels.3dcontent");
        
        download = WWW.LoadFromCacheOrDownload(url, 0);
        yield return download;
        myLoadedAssetBundle = download.assetBundle;
        foreach (string assetName in myLoadedAssetBundle.GetAllAssetNames()) {
            Debug.Log(assetName);
        }
        //Debug.Log(myLoadedAssetBundle.mainAsset.name);
        GameObject obj = Instantiate(myLoadedAssetBundle.LoadAsset("Cube")) as GameObject;
    }


    /*IEnumerator InstantiateObject()

    {
        string assetBundleName = "";
        string uri = "file:///" + Application.dataPath + "/AssetBundles/" + assetBundleName;
        UnityWebRequest request = UnityWebRequest.GetAssetBundle(uri);
        yield return request.Send();
        AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);
        GameObject cube = bundle.LoadAsset<GameObject>("Cube");
        GameObject sprite = bundle.LoadAsset<GameObject>("Sprite");
        Instantiate(cube);
        Instantiate(sprite);
    }*/
}
