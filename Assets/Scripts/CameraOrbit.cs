﻿using UnityEngine;
using System.Collections;

public class CameraOrbit : MonoBehaviour
{
    //public GameObject LookTarget;
    public float MinDistance = 1.0f;
    public float MaxDistance = 1.3f;
    float distance = 3;
    float distanceTarget;
    Vector2 mouse;
    Vector2 mouseOnDown;
    Vector2 rotation;
    Vector2 target = new Vector2(Mathf.PI * 7 / 6, Mathf.PI / 5);
    //Vector2 target = new Vector2(0,0);
    public Vector2 targetOnDown;

    public bool moveTowardsDestination = false;
    public bool moveTowardsOrigin = false;

    public Transform targetCountry;

    // Use this for initialization
    void Start()
    {

        distanceTarget = transform.position.magnitude;
        //Debug.Log(distanceTarget);

        

    }
    public bool down = false;
    bool fadeOnce = true;

    float elapsedTime = 0;

    // Update is called once per frame
    void Update()
    {
        /*if (moveTowardsDestination)
        {
            float step =  Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, targetCountry.position, step);

            float dist = Vector3.Distance(transform.position, targetCountry.position);
            if (dist < 0.6f && fadeOnce)
            {
                fadeOnce = false;
                //GameObject.FindObjectOfType<FadeOut>().StartFade();
            }
        }
        else if (moveTowardsOrigin)
        {
            elapsedTime += Time.deltaTime;

            float step = Time.deltaTime;

            transform.position = Vector3.MoveTowards(transform.position, GameStateMode.instance.countryPos, step);

            float dist = Vector3.Distance(transform.position, GameStateMode.instance.countryPos);
            if (dist < Mathf.Epsilon)
            {
                distance = 1.2f;
                distanceTarget = 1.2f;
                target = GameStateMode.instance.targetOnDown;
                rotation = target;
                targetOnDown = GameStateMode.instance.targetOnDown;
                Debug.Log("Target : " + target);
                moveTowardsOrigin = false;
            }

        }
        else
        {
            
        }*/


        if (Input.GetMouseButtonDown(0))
        {

            mouseOnDown.x = Input.mousePosition.x;
            mouseOnDown.y = -Input.mousePosition.y;

            targetOnDown.x = target.x;
            targetOnDown.y = target.y;
            down = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            down = false;
        }
        if (down)
        {
            mouse.x = Input.mousePosition.x;
            mouse.y = -Input.mousePosition.y;

            float zoomDamp = distance / 1;

            target.x = targetOnDown.x + (mouse.x - mouseOnDown.x) * 0.001f * zoomDamp;
            target.y = targetOnDown.y + (mouse.y - mouseOnDown.y) * 0.001f * zoomDamp;

            //target.y = Mathf.Clamp(target.y, -Mathf.PI / 2 + 0.01f, Mathf.PI / 2 - 0.01f);
            //target.x = Mathf.Clamp(target.x, Mathf.PI / 2 - 0.01f, Mathf.PI * 3 / 2);
            //target.y = Mathf.Clamp(target.y, Mathf.PI / 5.3f + 0.01f, Mathf.PI / 2 - 0.01f);
        }

        distanceTarget -= Input.GetAxis("Mouse ScrollWheel");
        //Debug.Log("distanceTarget" + distanceTarget);
        distanceTarget = Mathf.Clamp(distanceTarget, MinDistance, MaxDistance);

        rotation.x += (target.x - rotation.x) * 0.1f;
        rotation.y += (target.y - rotation.y) * 0.1f;
        distance += (distanceTarget - distance) * 0.3f;
        Vector3 position;
        position.x = distance * Mathf.Sin(rotation.x) * Mathf.Cos(rotation.y);
        position.y = distance * Mathf.Sin(rotation.y);
        position.z = distance * Mathf.Cos(rotation.x) * Mathf.Cos(rotation.y);
        //Debug.Log(position);
        transform.position = position;
        transform.LookAt(Vector3.zero);

    }


}
